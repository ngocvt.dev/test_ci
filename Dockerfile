FROM golang:1.16.7-stretch as builder
LABEL maintainer='FelixVo <felix.vo@intelllex.com>'

ENV PROJECT="/mas-service"

WORKDIR ${PROJECT}
COPY . .

RUN mkdir /app-bin
ENV CGO_ENABLED=0
RUN go build -mod=vendor -o /app-bin ./...

FROM alpine:latest
WORKDIR /app
COPY --from=builder /app-bin/ .
COPY resources resources
ENV PATH="/app:${PATH}"

EXPOSE 3000

